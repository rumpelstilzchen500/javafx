package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class MyLabel extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane pane = new Pane();

        Label labelText= new Label("text??????");

        Image image = new Image(getClass().getResourceAsStream("/res/image2.jpg"));
        //System.out.println(getClass().getResource("../res/image2.jpg").getFile());


        ImageView img = new ImageView(image);
        img.setFitHeight(200);
        img.setFitWidth(300);

        Label imageLabel = new Label();
        imageLabel.setGraphic(imageLabel);
        imageLabel.setTranslateX(100);
        imageLabel.setTranslateY(100);


        pane.getChildren().addAll(labelText);


        Scene scene = new Scene(pane, 1000, 900);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
